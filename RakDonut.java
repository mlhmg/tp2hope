import java.io.*;
import java.util.*;

public class RakDonut{
    private static InputReader in;
    private static PrintWriter out;

    private static MyList rakDonutMap = new MyList();


    public static void main(String[] args) throws IOException{
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);


        int banyakBarisan = in.nextInt();
        for (int i = 1; i <= banyakBarisan; i++) {
            int banyakDonutdiBarisan = in.nextInt();
            MyList barisanDonut = new MyList();
            for (int j = 0; j < banyakDonutdiBarisan; j++) {
                int banyakChips = in.nextInt();
                barisanDonut.addLast(banyakChips);
            }
            rakDonutMap.addLast(barisanDonut);
        }
//        ((MyList)rakDonutMap.header.next.getValue()).addLast();
//        rakDonutMap.printList();
//        rakDonutMap.printList();

//        for (int i = 1; i <= banyakBarisan; i++) {
//            rakDonutMap.get(i).printList();
//        }

        int banyakPerintah = in.nextInt();
        for (int i = 0; i < banyakPerintah; i++) {
            String tipePerintah = in.next();
            ListNode current = rakDonutMap.header;
            ListNode currentTujuan = rakDonutMap.header;
            ListNode barisBerubah = new ListNode();
            switch (tipePerintah){
                case "IN_FRONT":
                    int banyakChipsMasukDepan = in.nextInt();
                    int barisanMasukDepan = in.nextInt();
//                    rakDonutMap.get(barisanMasukDepan).addFirst(banyakChipsMasukDepan);
                    for (int j = 0; j < barisanMasukDepan; j++) {
                        current = current.next;
                    }
                    ((MyList)current.getValue()).addFirst(banyakChipsMasukDepan);
                    barisBerubah = current;
                    break;
                case "OUT_FRONT":
                    int barisanKeluarDepan = in.nextInt();
                    for (int j = 0; j < barisanKeluarDepan; j++) {
//                        System.out.println(j);
                        current = current.next;
                    }
                    ((MyList)current.getValue()).removeFirst();
//                    System.out.println(3);

                    if (((MyList)current.getValue()).getFirstNode() == ((MyList)current.getValue()).tail){
//                        System.out.println(2);
                        current.prev.next = current.next;
                        current.next.prev = current.prev;
                    } else {
                        barisBerubah = current;
                    }

                    break;
                case "IN_BACK":
                    int banyakChipsMasukBlkng = in.nextInt();
                    int barisanMasukBlkng = in.nextInt();
                    for (int j = 0; j < barisanMasukBlkng; j++) {
                        current = current.next;
                    }
                    ((MyList)current.getValue()).addLast(banyakChipsMasukBlkng);
                    barisBerubah = current;
                    break;
                case "OUT_BACK":
                    int barisanKeluarBlkng = in.nextInt();
                    for (int j = 0; j < barisanKeluarBlkng; j++) {
//                        System.out.println(j);
                        current = current.next;
                    }
                    ((MyList)current.getValue()).removeLast();

                    if (((MyList)current.getValue()).getFirstNode() == ((MyList)current.getValue()).tail){
//                        System.out.println(2);
                        current.prev.next = current.next;
                        current.next.prev = current.prev;
                    } else {
                        barisBerubah = current;
                    }
                    break;
                case "MOVE_FRONT":
                    int asalBarisanDepan = in.nextInt();
                    int tujuanBarisanDepan = in.nextInt();
                    for (int j = 0; j < asalBarisanDepan; j++) {
//                        System.out.println(j);
                        current = current.next;
                    }
                    for (int j = 0; j < tujuanBarisanDepan; j++) {
//                        System.out.println(j);
                        currentTujuan = currentTujuan.next;
                    }

                    ((MyList)current.getValue()).getFirstNode().prev = ((MyList)currentTujuan.getValue()).header;
                    ((MyList)current.getValue()).getLastNode().next = ((MyList)currentTujuan.getValue()).getFirstNode();

                    ((MyList)currentTujuan.getValue()).header.next = ((MyList)current.getValue()).getFirstNode();
                    ((MyList)currentTujuan.getValue()).getFirstNode().prev = ((MyList)current.getValue()).getLastNode();

                    ((MyList)current.getValue()).header.next = null;
                    ((MyList)current.getValue()).tail.prev = null;

                    if (((MyList)current.getValue()).header.next == null){
//                        System.out.println(2);
                        current.prev.next = current.next;
                        current.next.prev = current.prev;
                    }
                    barisBerubah = currentTujuan;

                    break;
                case "MOVE_BACK":
                    int asalBarisanBlkng = in.nextInt();
                    int tujuanBarisanBlkng = in.nextInt();
                    for (int j = 0; j < asalBarisanBlkng; j++) {
//                        System.out.println(j);
                        current = current.next;
                    }
                    for (int j = 0; j < tujuanBarisanBlkng; j++) {
//                        System.out.println(j);
                        currentTujuan = currentTujuan.next;
                    }
                    ((MyList)current.getValue()).getFirstNode().prev = ((MyList)currentTujuan.getValue()).getLastNode();
                    ((MyList)current.getValue()).getLastNode().next = ((MyList)currentTujuan.getValue()).tail;

                    ((MyList)currentTujuan.getValue()).getLastNode().next = ((MyList)current.getValue()).getFirstNode();
                    ((MyList)currentTujuan.getValue()).tail.prev = ((MyList)current.getValue()).getLastNode();

                    ((MyList)current.getValue()).header.next = null;
                    ((MyList)current.getValue()).tail.prev = null;

                    if (((MyList)current.getValue()).header.next == null){
//                        System.out.println(2);
                        current.prev.next = current.next;
                        current.next.prev = current.prev;
                    }
                    barisBerubah = currentTujuan;
                    break;
                case "NEW":
                    int banyakChipsTambah = in.nextInt();
                    MyList barisBaru = new MyList();
                    barisBaru.addLast(banyakChipsTambah);
                    rakDonutMap.addFirst(barisBaru);
                    barisBerubah = rakDonutMap.getFirstNode();
                    break;
            }
//            System.out.println(barisBerubah);
            if(barisBerubah.getValue() != null){
                barisBerubah.prev.next = barisBerubah.next;
                barisBerubah.next.prev = barisBerubah.prev;


                ListNode pointerBaris = rakDonutMap.getFirstNode();
                while (true) {
                    if (pointerBaris.getValue() == null){
                        barisBerubah.prev = rakDonutMap.getLastNode();
                        barisBerubah.next = rakDonutMap.tail;

                        rakDonutMap.getLastNode().next = barisBerubah;
                        rakDonutMap.tail.prev = barisBerubah;
                        break;
                    }
                    if(((MyList)barisBerubah.getValue()).compareTo((MyList)pointerBaris.getValue()) == -1){
//                    out.println("a");
                        barisBerubah.prev = pointerBaris.prev;
                        barisBerubah.next = pointerBaris;

                        pointerBaris.prev.next = barisBerubah;
                        pointerBaris.prev = barisBerubah;
                        break;
                    }

                    pointerBaris = pointerBaris.next;

                }
            }

        }
        ListNode barisDonut = rakDonutMap.getFirstNode();

        while(barisDonut.next!=null){
            if (barisDonut.getValue() != null) {
//                    System.out.println(1);
                ListNode donut = ((MyList)barisDonut.getValue()).header.next;
                while(donut!=null){
//                        System.out.println(2);
                    if (donut.getValue() != null) {
//                            System.out.println(3);
                        out.print((int)donut.getValue()+" ");
                    }
                    donut = donut.next;
                }
            }
            out.println("");
            barisDonut = barisDonut.next;
        }
//        out.println("");
//        ListNode barisDonut = rakDonutMap.getFirstNode().next;
//        while(barisDonut.next!=null){
//            if (barisDonut.next.getValue() != null) {
//                ListNode donut = ((MyList)barisDonut.next.getValue()).header.next;
//                while(donut!=null){
//                    if (donut.getValue() != null) {
//                        out.print((int)donut.getValue()+" ");
//                    }
//                    donut = donut.next;
//                }
//            }
//            out.println("");
//            barisDonut = barisDonut.next;
//        }
//        out.close();
        out.close();
    }

    static class ListNode{
        ListNode next;
        ListNode prev;
        Object value;

        public ListNode(ListNode next, ListNode prev, Object value) {
            this.next = next;
            this.prev = prev;
            this.value = value;
        }
        public ListNode(){
            this.value = null;
            this.next = null;
            this.prev = null;
        }

        public Object getValue(){
            return value;
        }

        @Override
        public String toString() {
            return value+"";
        }


    }

    static class MyList{
        ListNode header;
        ListNode tail;

        public MyList(){
            this.header = new ListNode(tail, null, null);
            this.tail = new ListNode(null, header,null);
        }

        public void addFirst(Object value){
            ListNode temp;
            temp = new ListNode(this.header.next , this.header, value);
            this.header.next.prev = temp;
            this.header.next = temp;
        }

        public void addLast(Object value){
            ListNode temp;
            temp = new ListNode(this.tail, this.tail.prev, value);
            this.tail.prev.next = temp;
            this.tail.prev = temp;
        }

        public void removeFirst(){
            this.header.next = this.header.next.next;
            this.header.next.prev = this.header;
        }

        public void removeLast(){
            this.tail.prev = this.tail.prev.prev;
            this.tail.prev.next = this.tail;
        }

        public void printList(){
            ListNode now = this.header.next;
            while(now != this.tail){
                System.out.print(now.value+" ");
                now = now.next;
            }
            System.out.println("");
        }
        public ListNode getFirstNode(){
            return this.header.next;
        }

        public ListNode getLastNode(){
            return this.tail.prev;
        }

        public int compareTo(MyList otherBaris){
            ListNode tempNode1 = this.header.next;
            ListNode tempNode2 = otherBaris.header.next;
//            out.println(2);
            while(true){
//                out.println(3);
                if(tempNode1 == this.tail && tempNode2 !=otherBaris.tail){
                    return -1;
                }
                if(tempNode1 != this.tail && tempNode2 == otherBaris.tail){
                    return 1;
                }
                if(tempNode1 == this.tail ){
                    return 1;
                }
                if ((int)tempNode1.getValue() < (int)tempNode2.getValue()){
                    return -1;
                }
                if ((int)tempNode1.getValue() > (int)tempNode2.getValue()){
                    return 1;
                }

                tempNode1 = tempNode1.next;
                tempNode2 = tempNode2.next;
            }


        }

        @Override
        public String toString() {
            return "MyList{" +
                    "header=" + header.next +
                    ", tail=" + tail.prev +
                    '}';
        }
    }

    public static void printOutput(String answer) throws IOException {
        out.println(answer);
    }

    static class InputReader {
        // taken from https://codeforces.com/submissions/Petr
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

    }
}